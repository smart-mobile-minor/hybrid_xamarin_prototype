﻿using Prototype_Xamarin_SM7.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace Prototype_Xamarin_SM7.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}