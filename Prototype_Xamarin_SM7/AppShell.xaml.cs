﻿using Prototype_Xamarin_SM7.ViewModels;
using Prototype_Xamarin_SM7.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Prototype_Xamarin_SM7
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
            Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
        }

    }
}
